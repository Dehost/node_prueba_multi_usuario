var socket  = require( 'socket.io' );
var io = socket.listen(3000);

console.log('sdfsdfsd');

io.on('connection', function(socket){
  
  console.log('a user connected');

  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
  
  socket.on('send', function(data){
    io.sockets.emit('send', data);
  });

  socket.on('subscribe', function(room) {
    console.log('joining room', room);
    socket.join(room);
  });

  socket.on('message', function(data) {
      console.log('sending room post', data.room);
      socket.broadcast.to(data.room).emit('conversation private post', data);
  });

});
