<?php 
  function makeRandomString($max=6) {
    $i = 0; //Reset the counter.
    $possible_keys = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $keys_length = strlen($possible_keys);
    $str = ""; //Let's declare the string, to add later.
    while($i<$max) {
        $rand = mt_rand(1,$keys_length-1);
        $str.= $possible_keys[$rand];
        $i++;
    }
    return $str;
  }
?>

<!doctype html>
<html>
  <head>
    <title>Socket.IO chat</title>
    <style>
      * { margin: 0; padding: 0; box-sizing: border-box; }
      body { font: 13px Helvetica, Arial; }
      form { background: #ccc; padding: 3px; position: fixed; bottom: 0; width: 100%; }
      form #m { border: 0; padding: 10px; width: 90%; margin-right: .5%; }
      form button { width: 9%; background: rgb(130, 224, 255); border: none; padding: 10px; }
      #messages { list-style-type: none; margin: 0; padding: 0; }
      #messages li { padding: 5px 10px; }
      #messages li:nth-child(odd) { background: #eee; }

      #messages_private { list-style-type: none; margin: 0; padding: 0; }
      #messages_private li { padding: 5px 10px; }
      #messages_private li:nth-child(odd) { background: #eee; }
    </style>
  </head>
  <body>
    <ul id="messages">

    </ul>
    <ul id="messages_private">

    </ul>
    <form action="">
      <input id="send_private" type="text" placeholder="usuario send private" disabled />
      <hr/> <br/>
      <input type="text" id="n" placeholder="Mi nombre">
      <input id="m" type="text" autocomplete="off"></textarea>
      <button>Send</button>
    </form>

    <script src="node_modules/socket.io-client/dist/socket.io.js"></script>
    <script src="https://code.jquery.com/jquery-1.11.1.js"></script>

    <script>
      f = '<?php echo makeRandomString(); ?>';
      $(function () {
        socket = io.connect( 'http://'+window.location.hostname+':3000' );
        socket.emit('subscribe', f);
        
        $('form').submit(function(){
          if( $('#send_private').val() ){
            socket.emit('message', { name:$('#n').val(), msn: $('#m').val(), room:$('#send_private').val(), myid:f } );
            $('#send_private').val('');
          }else{
            socket.emit('send', { name:$('#n').val(), msn: $('#m').val(), myid:f } );
          }
          $('#m').val('');
          return false;
        });

        socket.on('send', function(data){
          $('#messages').append( $('<li>').html("<span id='name' data-id='"+data.myid+"'>"+data.name+'</span>:'+data.msn) );
        });

        socket.on('conversation private post', function(data) {
          $('#messages_private').append( $('<li>').html( "<span id='name' data-id='"+data.myid+"'>"+data.name+'</span>:'+data.msn) );
        });

        $(document).on('click', 'li>#name', function(){
          $("#send_private").val( $(this).data('id') );
        });

      });
    </script>
  </body>
</html>
    